let local = localStorage.length ? (JSON.parse(localStorage.getItem("notes"))) : [];
// localStorage.clear();

 function allowDrop (e){
    e.preventDefault()
 }

 let dragElId =null;


    class InputConstr {
        constructor( type = "", className = "", id = "", name = " ", placeholder = " ", value = "") {
            this._type = type;
            this._className = className;
            this._id = id;
            this._name = name;
            this._placeholder = placeholder;
            this._value = value;
            this.elem = null;
        }
        render() {
            this.elem = document.createElement("input");
            this.elem.type = this._type;
            this.elem.className = this._className;
            this.elem.id = this._id;
            this.elem.name = this._name;
            this.elem.setAttribute("placeholder", this._placeholder);
            this.elem.setAttribute("value",this._value)
            this.elem.style.margin = "5px";
            this.elem.style.width = "75%";
      
            return this.elem
        }
     
    }
    
class ModalWind {
    constructor(className, id) {
        this._className = className;
        this._id = id;
        this.elem = null;
    }
    render(container, btn) {
        this.elem = document.createElement("dialog");
        this.elem.id = this._id;
        this.elem.className = this._className;
        this.elem.innerHTML = `<div class="modal-content">
                                <span class="close">&times;</span>

                                </div>`;
       
        this.close();
        return this.elem;
    }
    open() {
        this.elem.classList.add("active")
    }
    close() {
        this.elem.querySelector(".close").addEventListener("click", () => this.elem.remove())
    }
}

class NoteModal extends ModalWind {
    constructor(action, id, dedlineHTML, headerHTML, descriptiontHTML,submitHTML, ...args) {
        super(...args);
        this.action = action;
        this.id = id;
        this.dedlineHTML = dedlineHTML;
        this.headerHTML = headerHTML;
        this.descriptiontHTML = descriptiontHTML;
        this.submitHTML = submitHTML;
        }
    render() {
        const form = document.createElement("form");
        form.action = this.action;
        form.id = this.id;
        form.append(this.dedlineHTML);
        form.append(this.headerHTML);
        form.append(this.descriptiontHTML);
        form.append(this.submitHTML); 
        
        const modal = super.render();
        modal.querySelector(".modal-content").append(form);
        
        return modal;
    }

}


class NewNoteWind{
    constructor ( className ="", id="", attribute ="true" ){
         this._className = className;
        this._id =id;
        this._attribute = attribute;
        this.elem = null;
    }
    render(){
        this.elem = document.createElement("li");
        this.elem.className = this._className;
        this.elem.id=this._id;
        this.elem.setAttribute("draggable", this._attribute); 
        this.elem.innerHTML=`<span id="removeCard" class="removeNote">X</span>
                            <div class="noteContent"> </div>
                            <button class="btn btn-info btn-sm ml-2 edit" id="editID${createID ()}">EDIT</button>                            
                           `
        this.removeCard();
        this.draggStart();
        this.getEditBtn();
      
        return  this.elem
    }

    removeCard(){
        const removeCard = this.elem.querySelector("#removeCard")
        
        removeCard.addEventListener( "click", function(e){
            e.target.parentNode.remove();
            localStorage.clear()
            let filtredLocal = local.filter(item =>item.liId != e.target.parentNode.id)
            localStorage.setItem("notes", JSON.stringify(filtredLocal));
            local = JSON.parse(localStorage.getItem("notes"));
 
        })
            }
        
            
        draggStart (){
            const dragCard = document.querySelectorAll("[draggable]");
            dragCard.forEach(item => item.addEventListener("dragstart", drag))  
        }
        
        
        // Изменение карты 
        getEditBtn (){
            const editBtn = this.elem.querySelectorAll("button");
           
                for (let i = 0; i < editBtn.length; i++){
                    console.log( editBtn[i]);
                    editBtn[i].addEventListener("click", (e)=>{
                        console.log(e.target.parentNode);
                        const editNote =local.filter(item => item.liId === e.target.parentNode.id)
                        
                        renderModal();

                        const formInputs = document.getElementById("modalFormNote").getElementsByTagName("input");

                        for (let i =0; i < formInputs.length; i++){
                          
                            for (let key in editNote[0]){
                                formInputs[i].classList.contains(key) && (formInputs[i].value = editNote[0][key])
                            }
                        }
                      
                        document.getElementById("modalSubmit").addEventListener("click", (e)=>{
                                                               
                                    
                            
                            const parentID = document.getElementById(this._id).parentNode.id;
                            const liId = this._id;

                            const editNote = {};
                            editNote["parentUL"] = parentID;
                            editNote["liId"]=liId;
                            const inputs = document.querySelectorAll("input");
                           
                            inputs.forEach(item => {
                               
                                (item.name !="SUBMIT") && (editNote[item.name] = item.value)
                                
                            })

                            const filtredLocal = local.filter(item => item.liId != liId )
                             console.log(filtredLocal)
                             filtredLocal.push(editNote);
                             console.log(filtredLocal)
                             localStorage.setItem("notes", JSON.stringify( filtredLocal))
                        })
                          
                        

                    });
                    
                }
          
            
        }     
    }

    const renderModal = ()=>{
                            const dedline = new InputConstr ( "datetime",
                                                            "dedline", 
                                                            "nodaldedlineId",
                                                            "dedline", 
                                                            "Dedline hh:mm day:mth");
                    const dedlineHTML = dedline.render();

                    const header = new InputConstr ( "text", 
                                                    "noteHaed",
                                                    `dedlineId${createID()}`,
                                                    "noteHaed",
                                                    "NOTE HEAD");
                    const headerHTML = header.render();

                    const descriptiont = new InputConstr ( "text",
                                                        "noteContent",
                                                        "modalDescriptiontId",
                                                        "noteContent",
                                                        "NOTE CONTENT");
                    const descriptiontHTML = descriptiont.render();

                    const submit = new InputConstr (  "submit",
                                                    "submit",
                                                    "modalSubmit",
                                                    "SUBMIT",
                                                    "submit",
                                                    "SUBMIT");
                    const submitHTML = submit.render();
                    const modalNote = new NoteModal ("", 
                                                    "modalFormNote",
                                                    dedlineHTML,
                                                    headerHTML,
                                                    descriptiontHTML,
                                                    submitHTML,
                                                    "modalNewNout",
                                                    "modalNewNoutID");

                    document.body.append(modalNote.render());

    }

    class NoteElem {
        constructor (tagName = "",className = "", tagValue="" )
        {
            this._tagName = tagName;
            this._className = className;
            this._tagValue = tagValue;
            this.elem = null;
        }
        render() {
            this.elem = document.createElement(this._tagName);
            this.elem.className = this._className;
            this.elem.innerText=this._tagValue;
            this.elem.style.margin = "5px";
            this.elem.style.width = "75%";
            
            return this.elem
        }
     
    };
    

    
    class NewNote extends NewNoteWind {
        constructor(id, dedlineHTML, noteHaedHTML, noteContentHTML, ...args) {
            super(...args);
            
            this._id = id;
            this._dedlineHTML = dedlineHTML;
            this._noteHaedHTML = noteHaedHTML;
            this._noteContentHTML = noteContentHTML;
           }
        render() {
            const div = document.createElement("div");
            div.id = this.id;
            div.append(this._dedlineHTML);
            div.append(this._noteHaedHTML);
            div.append( this._noteContentHTML); 
            
            const modal = super.render();
            modal.querySelector(".noteContent").append(div);
            
            return modal;
        }
    }

     

const createcardBtnEvList =(cardAria) =>{
    const findBtn = document.getElementById(cardAria);
    findBtn.addEventListener( "click", function(e){
                     renderModal();


        const modalNewNote = document.getElementById("modalFormNote");  
        modalNewNote.addEventListener("submit", (e)=>{
         
            e.preventDefault();

            const parentID = this.previousElementSibling.id;
            const newNote = {};
            newNote["parentUL"] = parentID;
            const inputs = document.querySelectorAll("input");
            inputs.forEach(item => {

                (item.name !="SUBMIT") && (newNote[item.name] = item.value)
                
            })
           
                document.getElementById("modalNewNoutID").remove();
                        

            const dedline = new NoteElem ("h4",
                                        "dedline",
                                        `${newNote.dedline}`);

            const dedlineHTML = dedline.render();

            const noteHaed = new NoteElem ("h5",
                                            "noteHaed",
                                            `${newNote.noteHaed}`);

            const noteHaedHTML = noteHaed.render();

            const noteContent = new NoteElem ("p",
                                              "noteContent",
                                              `${newNote.noteContent}`);

            const noteContentHTML = noteContent.render();
            
            const creatNewNote = new NewNote (`noteContentID${createID()}`,
                                                dedlineHTML,
                                                noteHaedHTML,
                                                noteContentHTML,
                                                "case",
                                                `noteID${createID()}`)
          
            this.previousElementSibling.appendChild(creatNewNote.render());
            draggStart (); 
            newNote["liId"] = this.previousElementSibling.lastChild.id;
          
            local.push(newNote);
            localStorage.setItem("notes", JSON.stringify( local))
            console.log(JSON.parse(localStorage.getItem("notes")))

        });
         
   
})
}

createcardBtnEvList("addBacklog");
createcardBtnEvList("addToDo");
createcardBtnEvList("addInProgerss");
createcardBtnEvList("addDone");


function createID (){
    return Math.floor(Math.random()*1000)
}

const criateNote =( parendId, liId, dedline, noteHaed, noteContent) =>{
                const dedline_criateNote= new NoteElem ("h4",
                                        "dedline",
                                        `${dedline}`);

            const dedlineHTML_criateNote = dedline_criateNote.render();

            const noteHaed_criateNote = new NoteElem ("h5",
                                            "noteHaed",
                                            `${noteHaed}`);

            const noteHaedHTML_criateNote = noteHaed_criateNote.render();

            const noteContent_criateNote = new NoteElem ("p",
                                            "noteContent",
                                            `${noteContent}`);

            const noteContentHTML_criateNote = noteContent_criateNote.render();

            const creatNewNote_criateNote = new NewNote (liId,
                                                dedlineHTML_criateNote,
                                                noteHaedHTML_criateNote,
                                                noteContentHTML_criateNote,
                                                `case`,
                                                `noteID${liId}`);
            document.getElementById(parendId).appendChild(creatNewNote_criateNote.render());

            

        }

        const storageLoading =JSON.parse(localStorage.getItem("notes"));
        storageLoading && storageLoading.forEach(item=>{
        criateNote( item.parentUL, item.liId, item.dedline, item.noteHaed, item.noteContent);
        draggStart ()

        })

        function draggStart (){
            const dragCard = document.querySelectorAll("[draggable]");
            dragCard.forEach(item => item.addEventListener("dragstart", drag))  
        }

        function allowDrop(ev){
            ev.preventDefault();
        }

        function drag (e) {
            console.log(e.target)
            const test = e.target.id
            console.log(test);
            e.dataTransfer.setData("idName", test);
             cdragElId = e.target.id;


             event.dataTransfer.setData("idName", event.target.id)
             
         }

         function drop (e){
              const data = e.dataTransfer.getData("idName");
           
            const TlimData = document.getElementById(data);
            console.log(data);
            e.target.closest("ul").appendChild(TlimData);
            const chosenNote = local.filter(item=> item.liId ===cdragElId)
            chosenNote[0].parentUL=e.target.closest("ul").id;
            localStorage.setItem("notes", JSON.stringify( local))
            
         }
        
        const backlog = document.getElementById("backlog");
        backlog.addEventListener("drop", drop);
        backlog.addEventListener("dragover", allowDrop);
        
        const toDo = document.getElementById("to-do");
        toDo.addEventListener("drop", drop);
        toDo.addEventListener("dragover", allowDrop);
        
        const inProgerss = document.getElementById("in-progerss");
        inProgerss.addEventListener("drop", drop);
        inProgerss.addEventListener("dragover", allowDrop);
        
        const done = document.getElementById("done");
        done.addEventListener("drop", drop);
        done.addEventListener("dragover", allowDrop);



